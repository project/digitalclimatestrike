<?php

namespace Drupal\digitalclimatestrike\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Digital Climate Strike' Block.
 *
 * @Block(
 *   id = "digitalclimatestrike_block",
 *   admin_label = @Translation("Digital Climate Strike block"),
 *   category = @Translation("Digital Climate Strike World"),
 * )
 */
class DigitalClimateStrikeBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#markup' => '<script src="https://assets.digitalclimatestrike.net/widget.js" async></script>',
    ];
  }

}
