# Digital Climate Strike (D8/9) Block Module

## README.md

## INTRODUCTION
This module adds a "Digital Climate Strike" participation block.
More info: https://digital.globalclimatestrike.net

##REQUIREMENTS
The "Digital Climate Strike" module needs an image field as requirement.

# INSTALLATION
Install via Drupal Administration or with the following composer commands 
in project root folder:
`$ composer require drupal/digitalclimatestrike`
and enable the module in your web folder:
`$ drush en digitalclimatestrike`

##CONFIGURATION

To add 'Digital Climate Strike' block you can go to Structure -> Block Layout 
(admin/structure/block) and click on 'Place Block' button associated 
with each available region.

## CHANGELOG
The first D8/D9 stable release is under the track.
